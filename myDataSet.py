from numpy.random import RandomState
import numpy

class MyDataSet(object):

    def __init__(self, images, labels, isTournament=False, name="Unknown"):
        """Construct a DataSet. one_hot arg is used only if fake_data is true."""
        #print('images.shape: %s labels.shape: %s' % (images.shape,labels.shape))
        assert images.shape[0] == labels.shape[0], (
            'images.shape: %s labels.shape: %s' % (images.shape,
                                                   labels.shape))
        self._num_examples = images.shape[0]
        self._isTournament = isTournament
        # Convert shape from [num examples, rows, columns, depth]
        # to [num examples, rows*columns] (assuming depth == 1)
        print images.shape
        #images = images.astype(numpy.float32)
        self._images = images
        self._labels = labels
        self._epochs_completed = 0
        self._index_in_epoch = 10000000
        self._name=name
        self._srnd=numpy.random.RandomState(42)
        
    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        assert self._isTournament == False
        return self._labels

    @property
    def t_ids(self):
        assert self._isTournament == True
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def getData(self):
        return self._images[:], self._labels[:]
    
    def next_batch(self, batch_size):
        """Return the next `batch_size` examples from this data set."""
            
        if batch_size >= self._num_examples:
            return self.images, self._labels
        start = self._index_in_epoch
        self._index_in_epoch += batch_size
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data
            if self._name=="Train":
                print "Shuffle!", self._name
            perm = numpy.arange(self._num_examples)
            self._srnd.shuffle(perm)
            self._images = self._images[perm]
            self._labels = self._labels[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size
        end = self._index_in_epoch
        return self._images[start:end], self._labels[start:end]

