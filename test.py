from PIL import Image, ImageFilter
import tensorflow as tf
import os

print os.path.dirname(os.path.realpath(__file__))

#print tf.gfile.FastGFile(fileDir, 'rb').read()
def imageprepare(filePath):
    runDir = os.path.dirname(os.path.realpath(__file__))
    fileFullPath = os.path.join(runDir, filePath)
    im = Image.open(fileFullPath)
    width = float(im.size[0])
    height = float(im.size[1])
    print width, "x", height
    newImage = Image.new('L', (64, 64), (255)) #creates white canvas of 28x28 pixels
    #if width > height: #check which dimension is bigger
        ##Width is bigger. Width becomes 20 pixels.
        #nheight = int(round((20.0/width*height),0)) #resize height according to ratio width
        #if (nheigth == 0): #rare case but minimum is 1 pixel
            #nheigth = 1  
        ## resize and sharpen
        #img = im.resize((20,nheight), Image.ANTIALIAS).filter(ImageFilter.SHARPEN)
        #wtop = int(round(((28 - nheight)/2),0)) #caculate horizontal pozition
        #newImage.paste(img, (4, wtop)) #paste resized image on white canvas
    #else:
        ##Height is bigger. Heigth becomes 20 pixels. 
        #nwidth = int(round((20.0/height*width),0)) #resize width according to ratio height
        #if (nwidth == 0): #rare case but minimum is 1 pixel
            #nwidth = 1
            ## resize and sharpen
        #img = im.resize((nwidth,20), Image.ANTIALIAS).filter(ImageFilter.SHARPEN)
        #wleft = int(round(((28 - nwidth)/2),0)) #caculate vertical pozition
        #newImage.paste(img, (wleft, 4)) #paste resized image on white canvas
    img = im.resize((28,28), Image.ANTIALIAS).filter(ImageFilter.SHARPEN)
    newImage.paste(img, (0, 0)) #paste resized image on white canvas
    #newImage.save("sample.png
    print newImage.size[0], newImage.size[1]
    newImage.show()
    tv = list(newImage.getdata()) #get pixel values

    #normalize pixels to 0 and 1. 0 is pure white, 1 is pure black.
    tva = [ (255-x)*1.0/255.0 for x in tv] 
    return tva
    #print(tva)
print imageprepare(im)
