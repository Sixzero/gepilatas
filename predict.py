import tensorflow as tf
import os
from os.path import isfile, join
import simpleCV_processing
import SimpleCV
from alexnet import alex_net
from PIL import ImageDraw
import time

print (__file__)
imData_x = list()
boxList = list()
#myPath = "Images/validation"
#onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
#for fileName in onlyfiles :
    #imData_x.append(image_preprocess.imageprepare(myPath + "/" + fileName))
TEST_EASY = True
imList = list()
myPath = "Images/testImages"
if TEST_EASY :
    myPath += "1"
onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]
print ("Going to load: {} picture.".format(len(onlyfiles)) )
for fileName in onlyfiles :
    im = SimpleCV.Image(myPath + "/" + fileName)
    if not TEST_EASY:
        im = simpleCV_processing.imgResize(im, 300)

    tmpBoxList = simpleCV_processing.getImageSliceBoxes(im, 44, 44, 8)
    myList = simpleCV_processing.getImageSlices(im, tmpBoxList)
    imData_x.append( myList)
    boxList.append( tmpBoxList)
    imList.append( im)
    print ("Pic num: {} loaded.".format(len(imList)))
# Parameters
learning_rate = 0.001
training_iters = 150

display_step = 1

# Network Parameters
n_input = 784 # MNIST data input (img shape: 28*28)
batch_size = 1
dropout = 0.8 # Dropout, probability to keep units


n_classes = 10 # MNIST total classes (0-9 digits)
inChannels = 3 # Szin csatornak szama
# tf Graph input
x = tf.placeholder(tf.float32, [None, n_input, inChannels], name="x_to_feed")
y = tf.placeholder(tf.float32, [None, n_classes])
keep_prob = tf.placeholder(tf.float32, name="keep_prob") # dropout (keep probability)



# Construct model
pred = alex_net(x, keep_prob)

# Define loss and optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Evaluate model
correct_pred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initializing the variables
init = tf.initialize_all_variables()

res = tf.argmax(pred,1)

with tf.Session() as sess:
    sess.run(tf.initialize_all_variables())
    
    saver = tf.train.Saver()
    saver.restore(sess, "model/model.ckpt")
    print ("Model restored.")
    colors = [(255, 0, 0, 200), (0, 0, 255, 200), (0, 255, 0, 200), (0, 0, 0, 200), (0, 255, 255, 200), (255, 255, 0, 200), (255, 0, 255, 200)]
    for p in range( len(imList) ) :
        timer = time.time()
        result = sess.run(res, feed_dict={x: imData_x[p], keep_prob: 1.0})
        print ("A futasido: {:.4}".format(time.time() - timer) )

        img = imList[p]
        facelayer = SimpleCV.DrawingLayer((img.width, img.height))
        for c, box in enumerate(boxList[p]):
            if result[c] != 3:
                facebox = facelayer.rectangle(box[0], box[1], colors[result[c]])

        img.addDrawingLayer(facelayer)
        img.applyLayers()
        img.show()
        time.sleep(1)
        #raw_input("Press Enter for EXIT")

        # draw = ImageDraw.Draw(imList[p])
        # for i in range(len(boxList[p])) :
        #     if result[i] != 3 :
        #         draw.rectangle(boxList[p][i], outline=colors[result[i]])
        # imList[p].show()
    
