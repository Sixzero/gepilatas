import cv2
from SimpleCV import Camera
import os
import time


# Initialize the camera
cam = Camera()
img = cam.getImage()
img.show()
while 1:

    # Get Image from camera
    img = cam.getImage()
    raw_input("Press Enter to continue...")
    img.save("Images/screenshots/" + str(time.time() ) )
    img.show()


    # k = cv2.waitKey(100)
    # if k == 27:  # Esc key to stop
    #     break
    # elif k == -1:  # normally -1 returned,so don't print it
    #     print k  # else print its value
    #     continue
    # else:
    #     print k  # else print its value

